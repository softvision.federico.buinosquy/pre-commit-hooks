from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import json
import re
import subprocess
import sys

TFVARS_JSON_ID                   = 'VARIABLE-PARAMS-TO-TFVARS'
TFVARS_JSON_PRE_COMMIT_HOOK_NAME = TFVARS_JSON_ID + ' PRE-COMMIT HOOK'
TFVARS_JSON_INDENT               = ' '

def get_terraform_input_vars(terraform_files):
  try:
    raw_json = subprocess.check_output(['terraform-docs', '--no-sort', '--with-aggregate-type-defaults', 'json'] + terraform_files)
  except subprocess.CalledProcessError:
    raw_json = '{ "Inputs": [] }'

  return json.loads(raw_json)

def process_terraform_input_vars(visitor, terraform_files):
  json = get_terraform_input_vars(terraform_files)

  result = []
  for input in json['Inputs']:
    result.append(visitor(input))
  return result

def transform_terraform_input_var_to_file(input):
  result = input['Name'] + '='
  if input['Default'] is not None:
    if type(input['Default']) == dict:
      # Was a default value provided?
      if input['Default']['Value'] != '':
        # Is it a complex value?
        if type(input['Default']['Value']) in [dict, list]:
          result += json.dumps(input['Default']['Value'])
        else:
          result += input['Default']['Value']
      else:
        if input['Type'] == 'list':
          result += ", '[]'"
        elif input['Type'] == 'map':
          result += ", '{}'"
    else:
      result += ", '%s'" % input['Default']

  if input['Description'] is not None:
    result += ' #  "%s"' % input['Description']
  return result

def generate_vars_content(terraform_files, varsfile_path):
  BEGIN_CONTENT_PLACEHOLDER = '// BEGINNING OF ' + TFVARS_JSON_PRE_COMMIT_HOOK_NAME
  END_CONTENT_PLACEHOLDER = '// END OF ' + TFVARS_JSON_PRE_COMMIT_HOOK_NAME

  content = ""
  for param in process_terraform_input_vars(transform_terraform_input_var_to_file, terraform_files):
    content += param+"\n"

  f = open(varsfile_path,'r+')
  result = re.sub(
             r"(" + BEGIN_CONTENT_PLACEHOLDER + ").*?(" + END_CONTENT_PLACEHOLDER + ")",
             r"\1" + "\n" + content + r"\2",
             f.read(),
             flags=re.DOTALL
  )

  f.seek(0)
  f.truncate()
  f.write(result)
  f.close()

def main(argv=None):
  parser = argparse.ArgumentParser()
  parser.add_argument('filenames', nargs='*', help='Filenames pre-commit believes have changed.'),
  parser.add_argument('-v', '--varsfile', default='./default.vars', help="The path to your vars file. Defaults to './default.vars'.")

  try:
    args = parser.parse_args(argv)
  except:
    parser.print_help()
    sys.exit(0)

  generate_vars_content(args.filenames, args.varsfile)

if __name__ == '__main__':
  exit(main())
