pre-commit-hook

Use-Case: Terraform Input Variables for generate a file with name = value #description format


Sample Usage
Add the hook to your .pre-commit-config.yaml

```
- repo: https://gitlab.com/softvision.federico.buinosquy/pre-commit-hooks.git
  rev: 1.1.0
  hooks:
  - id: gitlab_params_terraform_input_vars
```
  
2. Add placeholders to your project's default.vars
This hook places the variables inside the following BEGINNING OF … HOOK and END OF … HOOK placeholders:

```
// BEGINNING OF VARIABLE-PARAMS-TO-TFVARS PRE-COMMIT HOOK
// END OF VARIABLE-PARAMS-TO-TFVARS PRE-COMMIT HOOK
```

Testing
You can test the behavior , where TERRAFORM_MODULE_PATH is either a path to a Terraform module directory, or contains a sequence of files containing Terraform variable definitions. The results can then be seen in the project's default.vars.

```
git clone https://gitlab.com/softvision.federico.buinosquy/pre-commit-hooks.git
pre-commit try-repo ./pre-commit-hooks gitlab_params_terraform_input_vars --files $TERRAFORM_MODULE_PATH
```