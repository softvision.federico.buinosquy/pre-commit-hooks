from setuptools import find_packages
from setuptools import setup


setup(
  name='pre_commit_hooks',
  description='Some useful hooks for https://pre-commit.com.',
  url='https://gitlab.com/softvision.federico.buinosquy/pre-commit-hooks.git',
  version='1.0.0',

  author='Federico Buinosquy',
  author_email='fede85@gmail.com',

  classifiers=[
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 2',
    'Programming Language :: Python :: 2.7',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: Implementation :: CPython',
    'Programming Language :: Python :: Implementation :: PyPy',
  ],

  packages=find_packages(),
  entry_points={
    'console_scripts': [
      'gitlab_params_terraform_input_vars = pre_commit_hooks.gitlab_params_terraform_input_vars:main',
    ],
  },
)
